package com.example.demafayz.virtualizationremotecontroll.ui.view

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet

/**
 * Created by demafayz on 19/12/2017.
 */
class BaseToolViver : AppCompatTextView {

    var toolId : String? = null

    constructor(context: Context) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    fun addText(text : String) {
        this.text = "${this.text}\n$text"
    }
}