package com.example.demafayz.virtualizationremotecontroll.net

import org.java_websocket.client.WebSocketClient
import org.java_websocket.drafts.Draft
import org.java_websocket.handshake.ServerHandshake
import java.lang.Exception
import java.net.URI

/**
 * Created by demafayz on 17/12/2017.
 */
class Client : WebSocketClient {

    private var onMessage: OnMessage? = null
    private var onConnection: OnConnection ? = null

    constructor(serverUrl : URI) : super(serverUrl)

    constructor(serverUrl: URI, protocolDraft : Draft) : super(serverUrl, protocolDraft)

    override fun onOpen(handshakedata: ServerHandshake?) {
        println("opened connection")
        onConnection?.onConnectionSuccess(handshakedata)
    }

    override fun onClose(code: Int, reason: String?, remote: Boolean) {
        println("Connection closed by ${(if (remote) "remote peer" else "us")} Code: $code Reason: $reason")
    }

    override fun onMessage(message: String?) {
        if (!message.isNullOrEmpty()) {
            onMessage?.onMessage(message!!)
        }
    }

    override fun onError(ex: Exception?) {
        ex?.printStackTrace()
        onConnection?.onConnectionError(ex)
    }

    fun setOnMessage(onMessage: OnMessage) {
        this.onMessage = onMessage
    }

    interface OnMessage {
        fun onMessage(message : String)
    }

    fun setOnConnection(onConnection: OnConnection) {
        this.onConnection = onConnection
    }

    interface OnConnection {
        fun onConnectionSuccess(handshakedata: ServerHandshake?)
        fun onConnectionError(ex: Exception?)
    }
}