package com.example.demafayz.virtualizationremotecontroll.ui.view

import android.content.Context
import android.support.v7.widget.AppCompatButton
import android.util.AttributeSet
import java.util.jar.Attributes

/**
 * Created by demafayz on 17/12/2017.
 */

class BaseTool : AppCompatButton {

    var toolId : String? = null

    constructor(context: Context) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)


}