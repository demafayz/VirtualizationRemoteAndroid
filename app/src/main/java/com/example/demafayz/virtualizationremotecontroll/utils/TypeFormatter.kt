package com.example.demafayz.virtualizationremotecontroll.utils

/**
 * Created by demafayz on 22/12/2017.
 */
class TypeFormatter {

    companion object {
        fun strToFloatSafely(decimal : String) : Float {
            try {
                return decimal.toFloat()
            } catch (ex : NumberFormatException) {
                return 0F
            }
        }
    }
}