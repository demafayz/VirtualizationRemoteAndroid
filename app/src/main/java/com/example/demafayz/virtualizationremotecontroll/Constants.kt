package com.example.demafayz.virtualizationremotecontroll

/**
 * Created by demafayz on 17/12/2017.
 */

class Constants {

    object SOCKET {
        var PORT = 8080
        var PROTOCOL = "ws"
//        var ADDRESS = "localhost"
//        var ADDRESS = "192.168.56.255"
        var ADDRESS = "10.0.3.2"
        var URL = "$PROTOCOL://$ADDRESS:$PORT"
    }

    object TYPE {
        var VIRTUALIZATOR = "virtualizatior"
    }

    object ACTIONS {
        var TAG = "REMOTE_TAG"
        var CLICK = "click"
        var PROGRESS = "progress"
        var SCOPE = "scope"
        var BROKEN = "broken"
        var POWER = "power"
        var INPUT_WATER = "input_water"
        var INPUT_SOLD = "input_sold"
        var OUTPUT_SOLD = "output_sold"
    }

    object SLAG {
        var REMOTE = "remote"
        var SERVER = "server"
        var VIRTUALIZATOR = "virtualizatior"
    }

    object KEYS {
        var ID = "id"
        var ACTION = "action"
        var TYPE = "type"
        var VALUE = "value"
    }
}