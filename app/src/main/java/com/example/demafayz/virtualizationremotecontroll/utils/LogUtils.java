package com.example.demafayz.virtualizationremotecontroll.utils;

import android.util.Log;

import com.example.demafayz.virtualizationremotecontroll.BuildConfig;

/**
 * Created by demafayz on 11/12/2017.
 */

public class LogUtils {

    public static void d(Class clazz, String text) {
        if (BuildConfig.DEBUG) {
            Log.d(clazz.getSimpleName(), text);
        }
    }
}
