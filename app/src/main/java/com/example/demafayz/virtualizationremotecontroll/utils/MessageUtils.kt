package com.example.demafayz.virtualizationremotecontroll.utils

import android.view.View
import com.example.demafayz.virtualizationremotecontroll.Constants
import com.example.demafayz.virtualizationremotecontroll.ui.view.BaseTool
import org.json.JSONObject

/**
 * Created by demafayz on 17/12/2017.
 */
fun clickMessage(view : View) : String? {
    if (view is BaseTool) {
        val id = view.toolId
        return JSONObject()
                .put(Constants.KEYS.TYPE, Constants.SLAG.REMOTE)
                .put(Constants.KEYS.ACTION, Constants.ACTIONS.CLICK)
                .put(Constants.KEYS.ID, id).toString()
    }
    return null
}