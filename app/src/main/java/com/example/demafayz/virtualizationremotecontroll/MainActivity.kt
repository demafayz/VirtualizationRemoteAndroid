package com.example.demafayz.virtualizationremotecontroll

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.demafayz.virtualizationremotecontroll.data.EveporationData
import com.example.demafayz.virtualizationremotecontroll.net.Client
import com.example.demafayz.virtualizationremotecontroll.utils.TypeFormatter
import java.net.URI
import kotlinx.android.synthetic.main.activity_main_remove.*
import com.example.demafayz.virtualizationremotecontroll.utils.clickMessage;
import kotlinx.android.synthetic.main.activity_main.*
import org.java_websocket.handshake.ServerHandshake
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception

class MainActivity : AppCompatActivity(), View.OnClickListener, Client.OnMessage, Client.OnConnection {

    var client : Client? = null
    var eveporationData : EveporationData = EveporationData()
    var eveporationMessage : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        createSocket()
        initView()
    }

    private fun initView() {

        mRemoteContainer.visibility = View.GONE
        mConnect.visibility = View.VISIBLE
        mConnect.setOnClickListener(this)

        finalWatherContainer.toolId = "evaporationValve_0"

        valve_1.toolId = "evaporationValve_0"
        valve_1.setOnClickListener(this)

        valve_2.toolId = "pumpHeater_0"
        valve_2.setOnClickListener(this)

        valve_3.toolId = "pumpWater_0"
        valve_3.setOnClickListener(this)

        valve_4.toolId = "resultPump_0"
        valve_4.setOnClickListener(this)

        valve_5.toolId = "heater_0"
        valve_5.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        if (view == null) {
            return
        }
        if (view.id == mConnect.id) {
            createSocket()
            return
        }
        val message = clickMessage(view)
        if (!message.isNullOrEmpty()) {
            client?.send(message)
        }
    }

    private fun createSocket() {
        client = Client(URI(Constants.SOCKET.URL))
        client?.setOnConnection(this)
        client?.setOnMessage(this)
        client?.connect()
    }

    override fun onConnectionSuccess(handshakedata: ServerHandshake?) {
        runOnUiThread {
            kotlin.run {
                Toast.makeText(this, "Socket connection success", Toast.LENGTH_SHORT).show()
                mRemoteContainer.visibility = View.VISIBLE
                mConnect.visibility = View.GONE
            }
        }
    }

    override fun onConnectionError(ex: Exception?) {
        runOnUiThread {
            kotlin.run {
                Toast.makeText(this, "Socket connection error", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onMessage(message: String) {
        parceLogs(message)
        runOnUiThread {
            kotlin.run {
                updateContent()
            }
        }
    }

    private fun updateContent() {
        eveporationMessage = eveporationData.toString()
        println(eveporationMessage)
        finalWatherContainer.text = eveporationMessage
    }

    private fun parceLogs(message: String) {
        try {
            val jsonObj = JSONObject(message)
            val type = jsonObj.optString(Constants.KEYS.TYPE)
            if (!type.isNullOrBlank() && type == Constants.TYPE.VIRTUALIZATOR) {
                val action = jsonObj.optString(Constants.KEYS.ACTION)
                if (!action.isNullOrBlank() && action == Constants.ACTIONS.PROGRESS) {
                    val value = jsonObj.optString(Constants.KEYS.VALUE)
                    val progress = TypeFormatter.strToFloatSafely(value)
                    eveporationData.progress = progress
                    return
                }
                if (!action.isNullOrBlank() && action == Constants.ACTIONS.SCOPE) {
                    val value = jsonObj.optString(Constants.KEYS.VALUE).replace(" ML", "")
                    val scope = TypeFormatter.strToFloatSafely(value)
                    eveporationData.scope = scope
                    return
                }
                if (!action.isNullOrBlank() && action == Constants.ACTIONS.INPUT_WATER) {
                    val value = jsonObj.optString(Constants.KEYS.VALUE)
                    val message = "I:= WATER $value"
                    addLogToIOViewer(message)

                }
                if (!action.isNullOrBlank() && action == Constants.ACTIONS.INPUT_SOLD) {
                    val value = jsonObj.optString(Constants.KEYS.VALUE)
                    val message = "I:= SOLD $value"
                    addLogToIOViewer(message)
                }
                if (!action.isNullOrBlank() && action == Constants.ACTIONS.OUTPUT_SOLD) {
                    val value = jsonObj.optString(Constants.KEYS.VALUE)
                    val message = "O:= SOLD $value"
                    addLogToIOViewer(message)
                }
            }
        } catch (ex : JSONException) {
            ex.printStackTrace()
        }
    }

    private fun addLogToIOViewer(message: String) {
        runOnUiThread {
            kotlin.run {
                ioViewer.addText(message)
            }
        }
    }

    fun sendMessage(message : String) {
        client?.send(message)
    }
}
