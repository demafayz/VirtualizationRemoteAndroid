package com.example.demafayz.virtualizationremotecontroll.data

/**
 * Created by demafayz on 22/12/2017.
 */
data class EveporationData(var progress: Float = 0F, var scope: Float = 0F) {

    override fun toString(): String {
        return "Eveporation status:\n" +
                "progress:= ${progress * 1.0}\n" +
                "scope:= ${if (scope > 0) scope else {"Empty"}}"
    }
}